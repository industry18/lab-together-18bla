package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.List;

/**
 * Created by jwon117 on 16/05/2017.
 */
public class StudentInfoWriter {

    private File thisFile;

    public StudentInfoWriter(File thisFile) {
        this.thisFile = thisFile;
    }

    public void StudentRecords (List<Students> myList) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(thisFile))) {

            for (Students students : myList) {

                bw.write(students.toString());
            }
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
